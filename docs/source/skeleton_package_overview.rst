====================
The skeleton-package
====================

This is a blank python package with all the template/boilerplate code that goes into making a python package. There are some pytest illustrations detailing techniques for storing test data that is usable via pytest and example code. There is also a command line endpoint to demonstrate how that works. Versioning is implemented via bump2version and documentation using Sphinx, with jupyter-notebook integration. This package can be installed via pip like a real python package:

.. code-block::

    # From the root of skeleton-package
    python -m pip install .

Below is a list of all the files in the package and what they do:

Setup files
===========

``./VERSION``
-------------

A text file this simply contains the version number and nothing else. This is not 100% required. This is updated by bump2version you can read about that in `package management <package_management.md>`_.

``./contributors.txt``
----------------------

A text file that contains the names and e-mail addresses of people that have contributed to the project file with one name per line. **You will want to edit this file**.

``./LICENCE.txt``
-----------------

The licence for the project. I always use GNU General Public Licence v3 but other more permissive options are MIT. You can read about licences `here <https://choosealicense.com/licenses/>`_. If you change the licence file, do not forget to change the licence specification in ``setup.py``.

``./_version.py``
-----------------

The version as a python file, I think this is used by ``setup.py`` (but not sure??). This is updated by bump2version you can read about that in `package management <package_management.md>`_.

``./setup.py``
--------------

The main python setup file. This is where the package name and any command line endpoints are detailed. **These sections will need editing**:

.. code-block::

   NAME = 'skeleton_package'
   DESCRIPTION = 'Skeleton python package'
   URL = ''
   EMAIL = ''
   AUTHOR = ''
   REQUIRES_PYTHON = '>=3.7.0'
   VERSION = '0.1.0a1'

The VERSION variable is updated by bump2version you can read about that in `package management <package_management.md>`_. If you have altered the licence in anyway then this will need editing:

.. code-block::

    license='GPLv3+',
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: PyPy'
    ],

Finally you can add any command line programs to this section. It is essentially a package path all the way down to a function name (does not have to be main but that is the convention).

.. code-block::

    entry_points={
        "console_scripts": [
            "skeleton-cmd-line = skeleton_package.skeleton_module:main"
        ],
    },

``./MANIFEST.in``
-----------------

I am not 100% up to speed with the ``MANIFEST.in``. However, I do know it is used for including non-python files in the installed package. For that reason, ``include skeleton_package/example_data/example_datasets/*`` is included in the manifest. **You will need to update this from** ``skeleton_package`` **to whatever your package name is**. Also, to ensure the data files are installed make sure ``include_package_data=True`` is set in your ``setup()`` function in ``setup.py``. For more information see the `python-packaging docs <https://python-packaging.readthedocs.io/en/latest/non-code-files.html>`_.

``./requirements.txt``
----------------------

A set of package dependency requirements. Ideally your conda environment and ``requirements.txt`` should mirror each other. See `the pip documentation <https://pip.pypa.io/en/stable/user_guide/#requirements-files>`_ for more information on ``requirements.txt``.

``./.bumpversion.cfg``
----------------------

A configuration file for bump2version (bumpversion). **You will need to change the single reference to ``skeleton_package`` in this file to your own python package name**. This config file is detailed in `project admin <package_management.md>`_.

``./.gitignore``
----------------

A generic ``.gitignore`` file. You should not need to touch this unless you have anything special to hide from git.

``./README.md``
---------------

The project readme serves two functions. 1. as a gateway readme file when someone goes to the git repository. 2. it is also incorporated directly into the sphinx documentation through a linker file (``getting_started.md``), that is used to point to a file outside of the sphinx document root (``./docs/source``). **There are references to the** ``skeleton-package`` **that will need editing to your project name. Also, you will want to remove the section on installing the skeleton-package.**. The version in this file is updated by bump2version you can read about that in `package management <package_management.md>`_.


Python packages files
=====================

``./skeleton_package``
----------------------

A sub-directory that actually contains the source python module code. This should contain an ``__init__.py`` module and any other python code you have implemented. When you install the python package this is the point of the install. So in python you would do ``import skeleton_package``.

``./skeleton_package/__init__.py``
----------------------------------

Required for python to recognise ``skeleton_package`` as a package. This also contains the version number ``__version__``, that can be imported into any module, see ``./skeleton_package/skeleton_module.py`` for an example. Te version number is updated by bump2version you can read about that in `package management <package_management.md>`_.

``./skeleton_package/skeleton_module.py``
-----------------------------------------

The example module in ``skeleton_package``. Can be imported with ``from skeleton_package import skeleton_module``. It also implements a command line program. In your package you will want to delete this and put your own modules in here.

``./skeleton_package/example_data``
-----------------------------------

A sub-package for handling example data files. This has been designed so you can use the same example data files in both ``pytest`` and in example scripts. See `example data <example_data.rst>`_ for more information.

``./skeleton_package/example_data/__init__.py``
-----------------------------------------------

Required for python to recognise ``example_data`` as a sub package. i.e. ``from skeleton_package.example_data import examples``.

``./skeleton_package/example_data/examples.py``
-----------------------------------------------

A module that implements the example data functions and the logic for defining them. **There are references to** ``skeleton_package`` **in here that you will want to change**.

``./skeleton_package/example_data/example_datasets``
----------------------------------------------------

The location where all static data files serving example datasets should be located.

``./skeleton_package/example_data/example_datasets/string_data.txt``
--------------------------------------------------------------------

An example of a "dataset". If you delete this in your own package then remember to delete the corresponding load function ``dummy_load_data`` from ``./skeleton_package/example_data/examples.py``.

Resource files
==============

``./resources``
---------------

The resources directory contains all the files that are related to the project but not necessarily python files. This is my invention and not best practice as far as I know - I do not know if there is a best practice.

``./resources/examples``
------------------------

This directory will contain example code that the user can run to get an idea of how the package works. So this might contain python scripts and/or jupyter-notbooks. ``<LINK>``.

``./resources/examples/example_notebook.ipynb``
-----------------------------------------------

An example notebook that serves two purposes. The first is to demonstrate using ``example_data`` sub-package ``<LINK>``, the second is to illustrate calling an API function that exists in a command line script (so command line "scripts" can be used directly in python code). The third is to demonstrate, example code integration into Sphinx documentation. ``<LINK>``.

``./resources/conda_env``
-------------------------

Any conda environments that can be updated or built with the requirements for the project. These should be ``.yml`` files. Read `this <https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html?highlight=yml#creating-an-environment-from-an-environment-yml-file>`_ for information on conda ``.yml`` files and environments.

``./resources/conda_env/conda_create.yml``
------------------------------------------

The conda environment setup file will create a conda environment (in this case ``skeleton_package``) and then install all the dependencies (I have put a load that I usually use but you will need to update this and the environment name. See `getting started <getting_started.md>`_ for details of using the conda ``.yml`` files or the `conda documentation <https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html?highlight=yml#creating-an-environment-from-an-environment-yml-file>`_. Currently I have a separate create and update scripts. I am not sure if this is necessary, I could probably have a single one and either run ``conda create...`` for ``conda update...`` using it? You will want to edit this to match your ``./requirements.txt`` file.

``./resources/conda_env/conda_update.yml``
------------------------------------------

This will have the exact same information in it as the ``conda_create.yml`` but without the environment name. It is designed to update an existing environment. You will want to edit this to match your ``./requirements.txt`` file.

``./resources/bin``
-------------------

The bin directory is designed as a place where all non-python scripts can be located, so for example, bash scripts that might be associated with the project in someway. The thinking was that this will be a single directory the user can add to their ``$PATH``. However, there may be an official way of doing this via ``setup.py`` that I am unaware of?

``./resources/bin/README.md``
-----------------------------

A readme for the ``./resources/bin`` directory explaining it's usage. This is generic and nothing needs changing in here.

``./resources/pdf``
-------------------

A directory that will contain a single PDF copy of the sphinx documentation. This is built using the ``./resources/build/build_docs.sh`` script.

``./resources/build``
---------------------

This directory contains project admin files, used to increment the version number and built documentation (with instructions on doing both).

``./resources/build/build_docs.sh``
-----------------------------------

A bash script to build HTML and PDF sphinx documentation and to copy the PDF documentation over to the ``./resources/pdf`` directory. **You will need to edit this script and change the PDF documentation file name from** ``skeleton-package`` **to whatever your project is (line 30)**.

``./resources/build/README.md``
-------------------------------

A readme for the project admin. This will have some references to the project name ``skeleton_package`` (python package name) and ``skeleton-package`` (project name). that **will need to be edited to your package name**. This is also incorporated into the sphinx documentation via a linker file ``./docs/source/package_management.md``.

``./resources/build/inc_version.sh``
------------------------------------

A bash script that will increment the version number of the python package and the project. It will increment the version number in all the various places in the repository and then generate a git tag of the version number. It will do a dry run first before asking for permission to do it properly. It uses the ``./.bumpversion.cfg`` file for the locations of the version numbers. See, `project admin <package_management.md>`_ for more information. You do not need to edit anything in this file.

Test files
==========

``./tests``
-----------

All the ``pytest`` modules and configuration files should be located in here. Test modules have to start with the word ``test_`` and are usually named after the module they are testing against.

``./tests/test_skeleton_module.py``
-----------------------------------

A test module for out ``skeleton_module``. This has actual tests you can run in it (not all related to the module but a demonstration of how pytest works - and some usage strategies).

``./tests/pytest.ini``
----------------------

A configuration file for ``pytest`` I have not explored this much yet.

``./tests/conftest.py``
-----------------------

This contains re-usable test _fixtures_. These are functions that provide test data and are responsibly for setting up the data and tidying up the data (tear-down). This is imported direcly by pytest and does not need to be imported into your tests. This ``conftest.py`` demonstrates some feature ideas/strategies.

Documentation files
===================

``./docs``
----------

This central point of the sphinx documentation. I set sphinx up to have a separate ``source`` directory and ``build`` directory. The ``source`` directory will contain documentation files (such as this one) and the template to use in the building of the docs. It also contains ``conf.py`` which is the sphinx configuration. The build directory has html files and pdf build files. I usually commit the ``source`` directory to git (and the makefiles in the root of ``./docs`` but not the build directory.

``./docs/Makefile``
-------------------

A linux documentation make file call with ``make html``.

``./docs/make.bat``
-------------------

A windows documentation make file.

``./docs/source``
-----------------

This directory contains the Sphinx source documentation files that you will want to change to customise to your project.

``./docs/source/_static``
-------------------------

For building docs, you should not need to edit this.

``./docs/source/_templates``
----------------------------

A sphinx templates directory. You should not need to edit this.

``./docs/source/conf.py``
-------------------------

The shinx documentation configuration for the project. You will need to edit this section:

.. code-block::

   project = 'skeleton-package'
   copyright = '2021, Chris Finan'
   author = 'Chris Finan'

   # The full version, including alpha/beta/rc tags
   release = '0.1.0a1'

``./docs/source/index.rst``
---------------------------

The main documentation entry point. I have put a generic list in here. You will want to edit the ``skeleton-package`` reference in here.

``./docs/source/getting_started.md``
------------------------------------

A linker file to ``./README.md`` used in sphinx documentation. This is referenced from ``index.rst``.

``./docs/source/package_management.md``
---------------------------------------

A linker to ``./resources/build/README.md``. This is referenced from ``index.rst``.

``./docs/source/skeleton_package_overview.rst``
-----------------------------------------------

_This_ documentation file. This is referenced from ``index.rst``. You will want to delete this in your own package.


``./docs/source/contributing.rst``
----------------------------------

A generic document detailing the rules for contributing to the project. You may want to make this more specific but if not you will need to edit the ``root of the repository`` link and the ``contact us`` link. See `this <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`_ guide for links in restructured text. This is referenced from ``index.rst``.

``./docs/source/examples.rst``
------------------------------

A table of contents detailing all the example code used in the project. This is referenced from ``index.rst``.

``./docs/source/examples``
--------------------------

A directory that will hold documents relating to running the example code. These could be hand crafted ``.rst`` files for converted ``jupyter-notebooks``.

``./docs/source/examples/example_notebook.nblink``
--------------------------------------------------

An example of linking an example notebook located in ``./resources/examples`` into the sphinx documentation. This is a linker file pointing to the notebook. This requires a plugin to be active see ``./docs/source/conf.py``

``./docs/source/scripts.rst``
-----------------------------

A table of contents detailing all the scripts in the project, see below. This is referenced from ``index.rst``. **There is a reference to** ``skeleton_package`` **in here that you will want to change to your own package name.**

``./docs/source/scripts``
-------------------------

A directory to hold documentation about specific scripts (command line endpoints) implemented in the project and python package. See also ``./docs/source/scripts.rst``.

``./docs/source/scripts/bash_scripts.rst``
-------------------------------------------------

A placeholder for documentation on bash scripts implemented in the project

``./docs/source/scripts/python_scripts.rst``
---------------------------------------------------

A placeholder for documentation for python scripts (defined in ``./setup.py``) implemented in the project.

``./docs/source/api.rst``
-------------------------

A landing page for the API documentation. This will link out to specific package/sub-package documentation. In this case it links to a ``skeleton_package`` document ``skeleton_package.rst``. This is referenced from ``index.rst``.

``./docs/source/api``
---------------------

A directory that will hold API documentation. This is documentation that is taken directly from the docstrings in the various python modules in the project. This uses sphinx autodoc. I usually have a single ``.rst`` file for each package and sub-package in the project but you could have one for each module if you wanted.

``./docs/source/api/skeleton_package.rst``
------------------------------------------

Contains the ``automodule`` definition to allow sphinx to load the code the build the documentation from docstrings.
