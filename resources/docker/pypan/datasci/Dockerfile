FROM pandoc/minimal:latest-static as init
FROM python:3.9.13-alpine3.16 as build

ENV BUILD_DEPS \
    build-base \
    bzip2-dev \
    zlib-dev \
    xz-dev \
    openssl-dev \
    curl-dev \
    gfortran \
    openblas-dev

ENV PERSISTENT_DEPS \
    openblas \
    curl \
    git \
    sed

WORKDIR /data
COPY --from=init /pandoc /usr/local/bin/
COPY requirements.txt ./

RUN apk upgrade --update && \
    apk add --virtual .persistent-deps $PERSISTENT_DEPS
    apk add --virtual .build-deps $BUILD_DEPS && \
    pip install --upgrade pip && \
    pip install -r requirements.txt && \
    apk del .build-deps
