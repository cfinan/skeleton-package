bgen-reader>=4.0.8
biopython
bump2version
bx-python
crossmap
cyvcf2==0.30.18
# numpy>=1.18, <1.24
pandas>=2
pip
py2neo
pysam
pytest
pytest-dependency
requests
selenium
sqlalchemy<1.4
tqdm
zarr>=2.12
scipy
# numba==0.56.4
