#!/bin/bash
PROG_NAME="=== init-package ==="
BASE=$(basename "$0")
USAGE=$(
cat <<EOF
$PROG_NAME
Initialise an empty repo from the skeleton package and brand it up with the
correct package/import names.

The <repo_name> is often the same as the <import_name> but with hyphens instead
of underscores i.e skeleton-package might be the repo name and skeleton_package
might be the import name.

USAGE: $BASE [flags] <repo_name> <import_name> <repo_root>
EOF
)

FLAGS_HELP="$USAGE"

# Make sure the repo name is set
if [[ -z "$1" ]]; then
    echo "[error] no repo name defined" 2>&1
    echo "$USAGE"
    exit 1
fi

# Make sure the import name is set
if [[ -z "$2" ]]; then
    echo "[error] no import name defined" 2>&1
    echo "$USAGE"
    exit 1
fi

# Make sure the repo root directory is set
if [[ -z "$3" ]]; then
  echo "[error] no repo root defined" 2>&1
  echo "$USAGE"
  exit 1
fi

# exit on error and undef not allowed
set -eu

PROG_PATH=$(readlink -f "$0")
PROG_DIR=$(dirname "$PROG_PATH")
RESOURCE_DIR=$(dirname "$PROG_DIR")

# The repo and import name
REPO_NAME="$1"
IMPORT_NAME="$2"

# The root dir for the repo
REPO_ROOT_DIR=$(readlink -f "$3")
echo "$PROG_NAME"
echo "[info] repo root: $REPO_ROOT_DIR"
echo "[info] repo name: $REPO_NAME"
echo "[info] import name: $IMPORT_NAME"

# source in some common functions
# . "${PROG_DIR}"/common.sh

temp_dir=$(mktemp -d)
current_dir="$PWD"
cd "$temp_dir"
git clone https://gitlab.com/cfinan/skeleton-package.git
cd "$current_dir"
SKEL_ROOT="$temp_dir"/skeleton-package
echo "[info] skeleton root: $SKEL_ROOT"

# Make sure all the expected files and dirs are available in the skeleton
# package
# check_repo "$SKEL_ROOT"

echo "[info] about to copy skeleton-package to $REPO_ROOT_DIR"
echo "Press <ENTER> to build continue [CTRL-C to quit]..."
read var

# Now we can rsync the contents of the skeleton package over to to new repo
rsync --exclude ".git" -av "$SKEL_ROOT"/ "$REPO_ROOT_DIR"/

# Alter setup.py
sed -i "s/skeleton_package/${IMPORT_NAME}/;s/Skeleton/${REPO_NAME}/;s/skeleton-package/${REPO_NAME}/" "${REPO_ROOT_DIR}"/setup.py
# sed -i 's/"skeleton-cmd-line = skeleton_package\.skeleton_module:main"//' "${REPO_ROOT_DIR}"/setup.py

# bump2version config file
sed -i "s/<python_package>/${IMPORT_NAME}/" "${REPO_ROOT_DIR}"/.bumpversion.cfg

# Update the manifest file to add the example data
sed -i "s/skeleton_package/${IMPORT_NAME}/" "${REPO_ROOT_DIR}"/MANIFEST.in

# The project README
cp "${REPO_ROOT_DIR}"/README.md "${REPO_ROOT_DIR}"/checklist.md
sed -i "s/## The skeleton-package.*__version__/__version__/g" "${REPO_ROOT_DIR}"/README.md
sed -i "s/skeleton-package/${REPO_NAME}/g" "${REPO_ROOT_DIR}"/README.md
sed -i "s/skeleton_package/${IMPORT_NAME}/g" "${REPO_ROOT_DIR}"/README.md

# The doctring in examples.py
sed -i "s/skeleton_package/${IMPORT_NAME}/g" "${REPO_ROOT_DIR}"/skeleton_package/example_data/examples.py

# Sort out the conda environments
sed -i "s/skeleton_package/${IMPORT_NAME}/g" "${REPO_ROOT_DIR}"/resources/conda/envs/py*/conda_create.yaml

# Sort out the conda builds
sed -i "s/skeleton_package/${IMPORT_NAME}/g" "${REPO_ROOT_DIR}"/resources/conda/build/py*/meta.yaml
sed -i "s/skeleton-package/${REPO_NAME}/g" "${REPO_ROOT_DIR}"/resources/conda/build/py*/meta.yaml
sed -i "s/skeleton-package/${REPO_NAME}/g" "${REPO_ROOT_DIR}"/resources/conda/build/py*/run_test.sh
sed -i "s/skeleton_package/${IMPORT_NAME}/g" "${REPO_ROOT_DIR}"/resources/conda/build/py*/run_test.sh

# The Sphinx docs
sed -i "s/skeleton-package/${REPO_NAME}/" "${REPO_ROOT_DIR}"/docs/source/conf.py
sed -i "s/skeleton-package/${REPO_NAME}/" "${REPO_ROOT_DIR}"/docs/source/index.rst
sed -i "s/skeleton_package_overview//" "${REPO_ROOT_DIR}"/docs/source/index.rst
sed -i "s/skeleton_package/${IMPORT_NAME}/" "${REPO_ROOT_DIR}"/docs/source/scripts.rst

# The bumpversion config
sed -i "s/skeleton_package/${IMPORT_NAME}/" "${REPO_ROOT_DIR}"/.bumpversion.cfg

# CI-CD
sed -i "s/skeleton_package/${IMPORT_NAME}/" "${REPO_ROOT_DIR}"/resources/ci_cd/run_docker.sh
sed -i "s/skeleton_package/${IMPORT_NAME}/" "${REPO_ROOT_DIR}"/resources/ci_cd/pages.sh

# Delete the package-admin README
rm "${REPO_ROOT_DIR}"/resources/README.md

# Delete the contents of the bin dir
rm "${REPO_ROOT_DIR}"/resources/bin/*.sh
rm -r "${REPO_ROOT_DIR}"/resources/docker

# Skeleton package specific docs
rm "${REPO_ROOT_DIR}"/docs/source/skeleton_package_overview.rst

# Now rename the python root import dir and the module within it
mv "${REPO_ROOT_DIR}""/skeleton_package/skeleton_module.py" "${REPO_ROOT_DIR}"/skeleton_package/"$IMPORT_NAME".py
mv "${REPO_ROOT_DIR}""/skeleton_package" "${REPO_ROOT_DIR}"/"$IMPORT_NAME"

# Now rename some documentation files
mv "${REPO_ROOT_DIR}""/docs/source/api/skeleton_package.rst" "${REPO_ROOT_DIR}""/docs/source/api/${IMPORT_NAME}.rst"
rm "${REPO_ROOT_DIR}""/docs/source/data_dict/"db-schema-tables.rst
rm "${REPO_ROOT_DIR}""/docs/source/data_dict/"stupid-cmd-line-program.rst

# Now sort out the docker builds (for building in CI if needed)
mv "${REPO_ROOT_DIR}"/resources/docker/pypan/datasci "${REPO_ROOT_DIR}"/resources/docker/pypan/build
rm -r "${REPO_ROOT_DIR}"/resources/docker/pypan/base
mv "${REPO_ROOT_DIR}"/resources/docker/pypan "${REPO_ROOT_DIR}"/resources/docker/"${IMPORT_NAME}"

echo "*** END ***"
