# Package management
Here is documentation on the maintenance of the package. This is not really geared towards end users but rather a log of how version numbering is handled and how documentation is built and maintained. Throughout this section `./` indicates the root of the repository.

## Building the documentation
Sphinx is used to document the package and the documentation is maintained as a set of HTML files and a combined PDF document. The HTML pages use the readthedocs [theme](https://sphinx-rtd-theme.readthedocs.io/en/stable/). This can be installed with:

```
pip install sphinx-rtd-theme
pip install sphinx-bootstrap-theme
```

There are several other packages that are required for building and handling markdown ([myst-parser](https://myst-parser.readthedocs.io/en/latest/using/intro.html#installation))

```
pip install myst-parser
```

Handling jupyter-notebook inclusion into the documentation ([nbsphinx](https://nbsphinx.readthedocs.io/en/0.8.0/))

```
pip install nbsphinx
```

Associated with nbsphinx is [nbssphinx-link](https://pypi.org/project/nbsphinx-link/) which allows notebooks to be included in the documentation when they are not physically located inside the Sphinx build directory.

The documentation configuration and static HTML files are located in `./docs`.

In order to build the PDF documentation, you will need `pdftex`, this can be installed with, see [here](https://www.sphinx-doc.org/en/1.8/usage/builders/index.html#sphinx.builders.latex.LaTeXBuilder):
```
sudo apt-get install texlive-latex-recommended
sudo apt-get install texlive-fonts-recommended
sudo apt-get install texlive-latex-extra
sudo apt-get install latexmk
```

### Initialisation
This only needs to be done once to create the directory structure for Sphinx to work in. Change into the `./docs` directory and run `sphinx-quickstart` then answer the questions. Here, separate build and source directories were created.

```
$ sphinx-quickstart
Welcome to the Sphinx 3.4.0 quickstart utility.

Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).

Selected root path: .

You have two options for placing the build directory for Sphinx output.
Either, you use a directory "_build" within the root path, or you separate
"source" and "build" directories within the root path.
> Separate source and build directories (y/n) [n]: y

The project name will occur in several places in the built documentation.
> Project name: skelton-package
> Author name(s): Chris Finan
> Project release []: 0.1.0a1

If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.

For a list of supported codes, see
https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.
> Project language [en]: 

Creating file ./skelton-package/docs/source/conf.py.
Creating file ./skelton-package/docs/source/index.rst.
Creating file ./skelton-package/docs/Makefile.
Creating file ./skelton-package/docs/make.bat.

Finished: An initial directory structure has been created.

You should now populate your master file skelton-package/docs/source/index.rst and create other documentation
source files. Use the Makefile to build the docs, like so:
   make builder
where "builder" is one of the supported builders, e.g. html, latex or linkcheck.
```

### Configuration
Ensure that the Sphinx configuration file (`./docs/conf.py`) is updated to load all the required extensions. The config below uses the read the docs theme but you can use other themes as well.

```python
extensions = [
    "sphinx_rtd_theme",
    "myst_parser",
    "nbsphinx",
    "nbsphinx_link",
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    'sphinx.ext.todo',
    'sphinx.ext.viewcode'
]
```

And the theme:

```python
html_theme = 'sphinx_rtd_theme'
```

### Building HTML
The HTML documentation can be built with:
```

./docs/make html
```

### Building PDF
The PDF documentation can be built with:
```
./docs/make latexpdf
```

The resulting pdf file is located at `skelton-package/docs/build/latex/skelton-package.pdf`. This is copied to `./resources/pdf/skelton-package.pdf`.
## Versioning
skelton-package uses 3 level versioning:

1. Major version number
2. Minor version number
3. Patch version number

For all non-stable versions (which is all of them at present), these are augmented with a:

* release - either `a` (alpha), `b` (beta), `prod` (production) - `dev` and `rc` (release candidate) were removed as I never used them.
* build number, this build number is associated with a release

So, an alpha version number will look like `0.2.0a0`

While skelton-package is unfinished, the versioning will be slightly ad-hoc but will stabilise once the minimal code base is in place.

skelton-package uses [`bump2version`](https://github.com/c4urself/bump2version) to increment the version numbers throughout the package. The version numbers are located in several different places (a `.` denotes the root of the repository):

* `./.bumpversion.cfg`
* `./README.md`
* `./skeleton_package/__init__.py`
* `./setup.py`
* `./VERSION`
* `./docs/source/conf.py`
* `./resources/conda/build/py*/meta.yaml`

The bumpversion config file is shown below but is also a hidden file in the root of the repository:
```
[bumpversion]
current_version = 0.2.0dev0
commit = True
tag = True
parse = (?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)(\-?(?P<release>[a-z]+)(?P<build>\d+))?
serialize = 
	{major}.{minor}.{patch}{release}{build}
	{major}.{minor}.{patch}

[bumpversion:part:release]
optional_value = prod
first_value = a
values = 
	a
	b
	prod

[bumpversion:part:build]

[bumpversion:file:VERSION]

[bumpversion:file:./README.md]
search = __version__: `{current_version}`
replace = __version__: `{new_version}`

[bumpversion:file:./_version.py]
search = __version__ = '{current_version}'
replace = __version__ = '{new_version}'

[bumpversion:file:./skeleton_package/__init__.py]
search = __version__ = '{current_version}'
replace = __version__ = '{new_version}'

[bumpversion:file:./setup.py]
search = VERSION = '{current_version}'
replace = VERSION = '{new_version}'

[bumpversion:file:./docs/source/conf.py]
search = release = '{current_version}'
replace = release = '{new_version}'

[bumpversion:file:./resources/conda/build/py37/meta.yaml]
search = version: {current_version}
replace = version: {new_version}

[bumpversion:file:./resources/conda/build/py38/meta.yaml]
search = version: {current_version}
replace = version: {new_version}

[bumpversion:file:./resources/conda/build/py39/meta.yaml]
search = version: {current_version}
replace = version: {new_version}

[bumpversion:file:./resources/conda/build/py310/meta.yaml]
search = version: {current_version}
replace = version: {new_version}
```

The procedure for incrementing the version number is as follows. This assumes that the `patch` number is being bumped and you are located in the root of the repository (where your `.bumpconfig.cfg` file is located):

1. run `bump2version` in "dry-run" (`-n`) mode with `--verbose` to make sure everything is ok:

```
$ bump2version --verbose -n patch
current_version=0.2.0dev0
commit=True
tag=True
parse=(?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)(\-?(?P<release>[a-z]+)(?P<build>\d+))?
serialize=
{major}.{minor}.{patch}{release}{build}
{major}.{minor}.{patch}
new_version=0.2.1dev0
```

2. If it all looks good (which it does above) then run for real:

```
$ bump2version --verbose patch
current_version=0.2.0dev0
commit=True
tag=True
parse=(?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)(\-?(?P<release>[a-z]+)(?P<build>\d+))?
serialize=
{major}.{minor}.{patch}{release}{build}
{major}.{minor}.{patch}
new_version=0.2.1dev0
```

3. Push to git. `bump2version` will produce git tags (imagine these are bookmarks in your repository). Not sure these are updated in your repository I think you have to push with the `--tags` flag.

```
git push --tags origin master
```
