"""A module illustrating a command line endpoint and python API access to the
 same function

Notes
-----
You can flesh out the module level docstring if you want. It will appear in
the API documentation.
"""
# Importing the version number (in __init__.py) and the package name
from skeleton_package import (
    __version__,
    __name__ as pkg_name,
    # You can also use pyaddons.log, in which case delete
    # skeleton_package/log.py
    log
)
# For handing data base configuration options within config files
# Delete if not needed
from sqlalchemy_config import config as cfg
import argparse
# Read/write CSV files in Python, delete if not needed
import csv
import sys
import os
# import pprint as pp

# The name of the script
_SCRIPT_NAME = "stupid-cmd-line program"
"""The name of the script (`str`)
"""
# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""

# Here are some useful variables for SQLAlchemy
try:
    _DEFAULT_CONFIG = os.path.join(os.environ['HOME'], '.db.cnf')
    """The default fallback path for the config file, root of the home
    directory (`str`)
    """
except KeyError:
    # HOME environment variable does not exist for some reason
    _DEFAULT_CONFIG = None

_DEFAULT_SECTION = 'db'
"""The default section name in an uni config file that contains SQLAlchemy
connection parameters (`str`)
"""
_DEFAULT_PREFIX = 'db.'
"""The default prefix name in an ini config section each SQLAlchemy connection
parameter should be prefixed with this (`str`)
"""

# Useful CSV options
_DELIMITER = "\t"
"""The default delimiter for the ``table_info`` and ``column_info`` files
(`str`).
"""

# Make sure csv can deal with the longest lines possible
csv.field_size_limit(sys.maxsize)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``something else``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    # Use logger.info to issue log messages
    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the
    # database
    sm = cfg.get_new_sessionmaker(
        args.infiles[0], conn_prefix=_DEFAULT_PREFIX,
        config_arg=args.config, config_env=None,
        config_default=_DEFAULT_CONFIG, exists=True
    )
    # Get the session to query/load
    session = sm()

    try:
        run_stupid_function(
            args.multiply_this, args.by_these[0], verbose=bool(args.verbose)
        )
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        session.close()
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'multiply_this',
        type=str,
        help="A value (treated as a string) to multiply"
    )
    parser.add_argument(
        '-db', type=str,
        help="An SQLALchemy connection string or a section in a configuration"
        " file containing the connection information. You can use this to"
        " prevent giving passwords on the command line. The default config "
        "file location is ``~/.db.ini``. You can change this with the "
        "``-config`` argument."
    )
    parser.add_argument(
        '--config', type=str, default=None,
        help="An alternating database configuration file location."
    )
    parser.add_argument(
        '-b', '--by-these',
        type=str,
        default=["A"],
        nargs="+",  # 1 or more arguments
        help="An optional list of space separated args. 'multiply_this' will"
        " be replicated by the number of arguments to '--by-these'"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="give more output, use -vv for progress monitoring"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_stupid_function(arg1, arg2, verbose=False):
    """The API entry point for running the command line program.

    This means that anyone wanting to run the program from their python code
    can call this function instead of _main(). This is also handy for calling
    with ``pytest``.

    Parameters
    ----------
    arg1 : `str`
        A string. This string will be multiplied by the length of the list
    arg2 : `list`
        A list. The list length will be used to multiply the string
    vervose : `bool`, optional, default: `False`
        Tell the user what I am doing
    """
    # The call to getLogger will return the same logger object if the name is
    # the same
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)
    logger.info("running program...")

    print("The result is: {0}".format(docstring_illustration(arg1, arg2)))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def docstring_illustration(arg1, arg2):
    """An example numpy docstring to illustrate the sections. The format is a
    numpy docstring.

    Parameters
    ----------
    arg1 : `str`
        A string. This string will be multiplied by the length of the list
    arg2 : `list`
        A list. The list length will be used to multiply the string

    Returns
    -------
    string_by_list : `str`
        Te ``arg1`` argument that .

    Raises
    ------
    IndexError
        If the list or string has no length.

    Notes
    -----
    This function is stupid. Any text or links should follow
    `reStructuredText <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`_
    (reST) format.

    Examples
    --------
    A simple test:

    >>> docstring_illustration("hello", ['A', 'B'])
    >>> 'hellohello'
    """
    if len(arg1) == 0 or len(arg2) == 0:
        raise IndexError("arguments must have some length")
    return arg1 * len(arg2)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
